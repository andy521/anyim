package com.anoyi.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 全局自定义属性配置
 */
@Configuration
@ConfigurationProperties("custom")
@Data
public class CustomAttributesConfig {

    // github 地址
    private String githubUrl;

    // 其他代码仓库地址
    private String gitlabUrl;

    // qq
    private String qq;

    // ICP 备案号
    private String icp;

}
